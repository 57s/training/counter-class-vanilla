import Counter from './Counter.js';

const CounterOne = new Counter('page');
const CounterTwo = new Counter('page');

CounterOne.render();
CounterTwo.render();
