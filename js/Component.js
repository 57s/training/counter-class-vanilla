class Component {
	constructor(rootClass) {
		this.elementRoot = document.querySelector(`.${rootClass}`);
	}

	render(Component) {
		this.elementRoot.appendChild(Component);
	}
}

export default Component;
