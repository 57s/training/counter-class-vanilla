import Component from './Component.js';

class Counter extends Component {
	constructor(parent) {
		super(parent);

		this.component = this.createCounter();
		this.elementValue = this.component.querySelector('.counter__number');
		this.elementButton = this.component.querySelector('.counter__button');

		this.elementButton.addEventListener('click', () => {
			this.incrementCounterNumber();
		});
	}

	value = 0;

	incrementCounterNumber = () => {
		this.value += 1;
		this.updateCounterNumber(this.value);
	};

	updateCounterNumber = value => {
		this.elementValue.textContent = value;
	};

	createCounter = () => {
		const warpCounter = document.createElement('div');
		warpCounter.classList.add('counter');

		const numberCounter = document.createElement('h3');
		numberCounter.classList.add('counter__number');
		numberCounter.textContent = '0';

		const buttonCounter = document.createElement('button');
		buttonCounter.classList.add('counter__button');
		buttonCounter.textContent = 'Прибавить';

		warpCounter.appendChild(numberCounter);
		warpCounter.appendChild(buttonCounter);

		return warpCounter;
	};

	render = () => {
		super.render(this.component);
	};
}

export default Counter;
